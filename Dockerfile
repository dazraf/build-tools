FROM ubuntu:18.04

LABEL MAINTAINER=@dazraf

ARG JAVA_VERSION=8
ARG MAVEN_VERSION=3.6.1
ARG DOCKER_CHANNEL=stable
ARG DOCKER_VERSION=18.09.6
ARG GCLOUD_VERSION=290.0.0

LABEL 	name="dazraf/build-tools" \
	description="OpenJDK $JAVA_VERSION Maven $MAVEN_VERSION NodeJS 12 and Docker in Docker!" \
	maintainer="dazraf" \
	url="https://gitlab.com/dazraf/build-tools"

RUN apt update -y
RUN apt upgrade -y
RUN apt install -y iputils-ping git vim bash zsh wget tar ca-certificates less gzip python-pip python-dev libffi-dev libssl-dev gcc g++ libc-dev make python3 hostname  
RUN chsh -s /usr/bin/zsh root
RUN wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O - | zsh || true

RUN wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
	chmod +x /usr/local/bin/gitlab-runner

RUN apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN apt-key fingerprint 0EBFCD88
RUN add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
RUN apt-get update -y
RUN apt-get install -y docker-ce docker-ce-cli containerd.io
RUN docker --version
RUN curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose
RUN docker-compose --version

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install -y openjdk-${JAVA_VERSION}-jdk 
RUN apt install -y nodejs
RUN curl -fsSL https://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | tar xzf - -C /usr/share
RUN mv /usr/share/apache-maven-$MAVEN_VERSION /usr/share/maven 
RUN ln -s /usr/share/maven/bin/mvn /usr/bin/mvn 
RUN set -eux && \
	useradd --create-home --no-log-init --shell /bin/bash gitlab-runner \
	&& echo 'gitlab-runner:gitlab-runner' | chpasswd \
	&& usermod -aG docker gitlab-runner \
	&& usermod -aG root gitlab-runner && \
	touch /var/run/docker.sock && \
	chmod 666 /var/run/docker.sock && \
	apt clean 

# gcloud and kubectl
RUN wget -q https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-$GCLOUD_VERSION-linux-x86_64.tar.gz && \
	tar zxvf google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz && \
	mv google-cloud-sdk /usr/local/share/
ENV PATH $PATH:/usr/local/share/google-cloud-sdk/bin
RUN /usr/local/share/google-cloud-sdk/bin/gcloud config set disable_usage_reporting true
RUN /usr/local/share/google-cloud-sdk/bin/gcloud components install kubectl

RUN gcloud components install beta --quiet

COPY modprobe.sh /usr/local/bin/modprobe
COPY docker-entrypoint.sh /usr/local/bin/
COPY gitlab-runner-run.sh /usr/local/bin/gitlab-runner-run

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64 
ENV MAVEN_HOME /usr/share/maven

WORKDIR /home/gitlab-runner

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["zsh"]





