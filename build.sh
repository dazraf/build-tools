#!/bin/sh

TARGET=dind-build-tools
VERSION=${CI_COMMIT_TAG}
BUILDNO=${CI_JOB_ID}
IMAGE=$TARGET:$VERSION.$BUILDNO

function exec_cmd()
{
    echo EXECUTE "$@"
    "$@"

    local rc=$?
    if [ $rc != 0 ]; then
        echo FAILED
        exit $rc
    fi
}

function build_release()
{
    exec_cmd docker build -t $IMAGE .
    echo DONE
}

build_release